// ignore_for_file: library_private_types_in_public_api, deprecated_member_use

import 'package:flutter/material.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:two_developer/login.dart';

void main() {
  runApp(const MyApp());
}
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Unit Converter',
      theme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: Colors.white,
      ),
      home: const SplashScreen(),
    );
  }
}

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
      splash: SizedBox(
        width: 300,
        height: 300,
        child: Column(
          children: [
            Image.asset('images/Splash_screen.png'),
          ],
        ),
      ), 
      backgroundColor: Colors.black,
      nextScreen: const LoginScreen(),
      splashTransition: SplashTransition.fadeTransition,
      );
  }
}

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

    TextEditingController txtName= TextEditingController();
    TextEditingController txtSurname = TextEditingController();
    TextEditingController txtAddress = TextEditingController();
    TextEditingController txtCellphone = TextEditingController();

    @override
    Widget build(BuildContext context) {
      return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          title: const Text('Change user details'),
          backgroundColor: Colors.transparent,
          ),
        body:
          Container(
            decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('images/backImage.jpg'),
                  fit: BoxFit.cover,
                  ),
            ),
            padding: const EdgeInsets.fromLTRB(50.0, 150.0, 50.0, 100.0),
            alignment: Alignment.center,
            child:
               Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: const <Widget>[
                      Text(
                      "Details",
                        style: TextStyle(fontSize:50.0,
                        color: Colors.grey,
                        fontWeight: FontWeight.w500,
                        fontFamily: "Roboto"),
                      )
                    ]
                  ),
                  const SizedBox(
                      width: 300,
                      height: 20,
                      ),
                  TextField(
                    controller: txtName,
                    keyboardType: TextInputType.emailAddress,
                    decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.account_box,),
                      border: OutlineInputBorder(),
                      hintText: "Name",
                    ),
                  ),
                  const SizedBox(
                      width: 300,
                      height: 10,
                    ),
                  TextField(
                    controller: txtSurname,
                    decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.account_box),
                      border: OutlineInputBorder(),
                      hintText: "Surname",
                    ),
                  ),
                  const SizedBox(
                      width: 300,
                      height: 10,
                    ),
                  TextField(
                    controller: txtAddress,
                    decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.home),
                      border: OutlineInputBorder(),
                      hintText: "Address",
                    ),
                  ),
                  const SizedBox(
                      width: 300,
                      height: 10,
                    ),
                    TextField(
                    controller: txtCellphone,
                    decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.phone),
                      border: OutlineInputBorder(),
                      hintText: "Cellphone",
                    ),
                  ),
                  const SizedBox(
                      width: 300,
                      height: 30,
                    ),
                  Opacity(opacity: 0.40,
                    child: SizedBox(
                      width: 200,
                        height: 30,
                      child: RaisedButton(key:null, onPressed:buttonPressed,
                        color: const Color(0xFFe0e0e0),
                        child:
                          const Text(
                          "Save",
                            style: TextStyle(fontSize:20.0,
                            color: Color(0xFF000000),
                            fontWeight: FontWeight.w100,
                            fontFamily: "Roboto"),
                          ),
                        ),
                    ),
                  ),
                ],
              ),
          ),
    
      );
    }
    void buttonPressed(){
      String name = txtName.text;
      String surname = txtSurname.text;
      String address = txtAddress.text;
      String cellphone = txtCellphone.text;
      
        showDialog(
        context: context,
        builder: (_) => const AlertDialog(
        title: Text('New details saved!'),
        content: Text('Saved'),
        ),
        barrierDismissible: true,
      );
      }
    }
    
